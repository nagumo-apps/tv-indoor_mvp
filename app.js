const dicionario = [
    //Frios
    {"cod":"006798", "nome":"Presunto Oval sem Capa Sadia"},
    {"cod":"005883", "nome":"Presunto Perdigão"},
    {"cod":"000963", "nome":"Presunto sem Capa Seara"},
    {"cod":"031257", "nome":"Mortadela Ouro Fatiada Perdigão"},
    {"cod":"089548", "nome":"Mortadela Defumada Seara"},
    {"cod":"009799", "nome":"Mortadela Bologna Ceratti"},
    {"cod":"231439", "nome":"Queijo Mussarela Fatiado"},
    {"cod":"010733", "nome":"Rosbife Ceratti"},
    {"cod":"010870", "nome":"Blanquet de Peru Sadia"},
    {"cod":"007252", "nome":"Salame Italiano Sadia"},
    
    //Peixaria
    {"cod":"037426", "nome":"Peixe Fresco Tilápia"},
    {"cod":"062114", "nome":"Peixe Fresco Corvina"},
    {"cod":"037266", "nome":"Peixe Fresco Pescada Branca"},
    {"cod":"062206", "nome":"Peixe Fresco Camarão Cinza"},
    {"cod":"038300", "nome":"Peixe Fresco Salmão filé"},
    {"cod":"036573", "nome":"Peixe Fresco Cação"},
    {"cod":"062541", "nome":"Peixe Fresco Filé Pescada Branca"},
    {"cod":"036429", "nome":"Peixe Atum"},
    {"cod":"037068", "nome":"Peixe Fresco Manjuba"},
    {"cod":"034456", "nome":"Filé de Tilápia"},

    //padaria
    {"cod":"021722", "nome":"Bolo sabor Cenoura com Chocolate"},
    {"cod":"073882", "nome":"Pudim de Leite Condensado com Coco"},
    {"cod":"011655", "nome":"Pão Italiano"},
    {"cod":"047746", "nome":"Pão Brioche Francesa"},
    {"cod":"042093", "nome":"Pão Francês (kg)"},
    {"cod":"066006", "nome":"Sonho De Creme"},
    {"cod":"083249", "nome":"Pão de Pizza sabor Frango"},
    {"cod":"089838", "nome":"Pão Chipa"},
    {"cod":"016230", "nome":"Carolina sabor Doce de Leite"},
    {"cod":"003872", "nome":"Pão de Leite"},

    //acougue
    {"cod": "078023", "nome":"Contra Filé a Vácuo Peça"},
    {"cod": "078382", "nome":"Bisteca Suína Congelada"},
    {"cod": "074766", "nome":"Coxa com Sobrecoxa Congelada"},
    {"cod": "077880", "nome":"Coxão Mole a Vácuo Peça"},
    {"cod": "089173", "nome":"Costela Minga"},
    {"cod": "207880", "nome":"Peito de Frango sem Osso Congelado"},
    {"cod": "000352", "nome":"Frango a Passarinho"},
    {"cod": "046480", "nome":"Acém sem Osso a Vácuo"},
    {"cod": "047401", "nome":"Paleta a Vácuo"},
    {"cod": "005487", "nome":"Costelinha Suína Congelada"},
    {"cod": "046244", "nome":"Coxão Mole a Vácuo Bife"},
    {"cod": "046213", "nome":"Contra Filé a Vácuo Bife"},
    {"cod": "046190", "nome":"Patinho a Vácuo Bife"},
    {"cod": "040877", "nome":"Pernil Suíno com Osso Congelado"},
    {"cod": "001625", "nome":"Carne Moída"},
    
    //salsicharia
    {"cod":"000581", "nome":"Salsicha Seara"},
    {"cod":"000604", "nome":"Salsicha Perdigão"},
    {"cod":"009836", "nome":"Salsicha Aurora"},
    {"cod":"000574", "nome":"Salsicha Sadia"},
    {"cod":"003599", "nome":"Linguiça Calabresa Defumada Aurora"},
    {"cod":"000512", "nome":"Linguiça Calabresa Perdigão"},
    {"cod":"007283", "nome":"Linguiça Calabresa Sadia"},
    {"cod":"052986", "nome":"Linguiça Toscana Na Brasa"},
    {"cod":"006095", "nome":"Linguiça Toscana Aurora"},
    {"cod":"009249", "nome":"Linguiça Toscana Sadia"},
    {"cod":"007177", "nome":"Linguiça de Frango Aurora"},

    //cafeteria
    {"cod":"09455", "nome":"Crepe Doce Avelã com Morango"},
    {"cod":"06315", "nome":"Pão na Chapa"},
    {"cod":"02198", "nome":"Brownie Fudge com Sorvete"},
    {"cod":"03153", "nome":"X-Salada"},
    {"cod":"09460", "nome":"X-Bacon"},
    {"cod":"09484", "nome":"Tapioca Romeu e Julieta"},
    {"cod":"09642", "nome":"Café Expresso Tradicional"},
    {"cod":"09646", "nome":"Chocolate Quente Nescau"},
    {"cod":"06631", "nome":"Coxinha"},
    {"cod":"09497", "nome":"Pão de Queijo"},
    {"cod":"204585", "nome":"Pastel de Carne"},
    {"cod":"07451", "nome":"Açaí 300ML"},
    {"cod":"08269", "nome":"Açaí 500ML"},
    {"cod":"09742", "nome":"Waffle com Sorvete"}
    ];


//captura os parametros da URL
const urlParams = new URLSearchParams(window.location.search);

const loja = urlParams.get("loja");
const setor = urlParams.get("setor");

//muda a imagem de fundo
document.querySelector("body").setAttribute("class", setor);

//recebendo os precos da api
const leobot = `http://ngo-nb-0052:3333/balanca/${loja}/${setor}`;

let codProd = "";
function pegaDescricao(codBalanca){
    dicionario.forEach(function(lista){

        if(lista.cod == codBalanca){
            codProd = lista.nome;
        }
    })
    return codProd;
}

//montando cabecalho
function mudaPeso(categoria){
     cabecalho = "";

    if(categoria == "padaria" || categoria == "frios"){
        cabecalho = `
        <tr>
            <!-- <th>Código</th> -->
            <th>Descrição</th>
            <th colspan="2">Preço/100g</th>
        </tr>
        `
    } else if(categoria == "cafeteria"){
        cabecalho = `
        <tr>
            <!-- <th>Código</th> -->
            <th>Descrição</th>
            <th colspan="2">Preço/Unidade</th>
        </tr>
        `
    } else {
        cabecalho = `
        <tr>
            <!-- <th>Código</th> -->
            <th>Descrição</th>
            <th colspan="2">Preço/kg</th>
        </tr>
        `
    };

    return cabecalho;
}

//montando a tabela de produtos
function modelo(codigo, descricao, preco, setor){

    if(codigo != "042093" && setor == "padaria" || setor == "frios"){
        precoDeGrama = parseFloat(preco) * 0.1;
    } 
    else {
        precoDeGrama = parseFloat(preco);
    }

    const precoFormat = precoDeGrama.toLocaleString('pt-BR')
    return `
    <tr>
        <!-- <td>${codigo}</td> -->
        <td>${descricao}</td>
        <td class="tabela--cifrao">R$</td>
        <td class="tabela--preco">${precoFormat}</td>
    </tr>
    `
};

function listarProduto(listaPreco){

    const cabecalho = mudaPeso(setor);
    document.getElementById("cabecalhoProduto").innerHTML = cabecalho;

    

    listaPreco.forEach(function(produto) {
        const codigo = produto.CODBALANCA;
        const preco = produto.PRECO;
        const descricao = pegaDescricao(codigo);
        const tabela = modelo(codigo, descricao, preco, setor);
        
        document.getElementById("listaProduto").innerHTML += tabela;
    })
}

function ListaDeProdutos(lista){
    let data = lista;

    let teste = data.filter(item => {
        console.log(item)
        return item
    })

}

fetch(leobot)
    .then(precos => precos.json())
    .then(listaPreco => ListaDeProdutos(listaPreco));

